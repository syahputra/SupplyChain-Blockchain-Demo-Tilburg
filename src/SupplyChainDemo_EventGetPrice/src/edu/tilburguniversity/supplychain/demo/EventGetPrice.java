package edu.tilburguniversity.supplychain.demo.SupplyChainDemo;

import edu.tilburguniversity.supplychain.demo.SupplyChainDemo.Quotation.*;

/**
 * The Event Objects for the event GetPrice().
 *
 */
public class EventGetPrice{

	public EventGetPrice() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventGetPrice other = (EventGetPrice) obj;
		return true;
	}

	@Override
	public String toString() {
		return "EventGetPrice []";
	}
}
