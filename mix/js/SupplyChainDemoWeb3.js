// file header
// contractVariable for Quotation
var QuotationContract = web3.eth.contract([
{"constant":true,"inputs":[],"name":"suppliermanagement","outputs":[{"name":"","type":"SupplierManagement"}],"type":"function"},
{"constant":true,"inputs":[],"name":"TransactionID","outputs":[{"name":"","type":""}],"type":"function"},
{"constant":true,"inputs":[],"name":"IssueDate","outputs":[{"name":"","type":""}],"type":"function"},
{"constant":true,"inputs":[],"name":"FreightTerms","outputs":[{"name":"","type":""}],"type":"function"},
{"constant":true,"inputs":[],"name":"PaymentTerms","outputs":[{"name":"","type":""}],"type":"function"},
{ "constant": false,
    "inputs": [],    
    "name": "AddNewQuotation",
    "outputs": [],
    "type": "function" }
,
  { "anonymous": false,
    "inputs": 
	[
	],    
    "name": "GetPrice",
    "type": "event"  }
]);   
// contractVariable for ShippingMethod
var ShippingMethodContract = web3.eth.contract([
{"constant":true,"inputs":[],"name":"warehouse","outputs":[{"name":"","type":"Warehouse"}],"type":"function"},

]);   
// contractVariable for Contract
var ContractContract = web3.eth.contract([
{"constant":true,"inputs":[],"name":"purchaseorder","outputs":[{"name":"","type":"PurchaseOrder"}],"type":"function"},

]);   


