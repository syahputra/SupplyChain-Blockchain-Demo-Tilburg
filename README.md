# APPENDIX

# A. Supply Chain Demo Illustrations

In the sample, we explain the use of blockchain in the supply chain. Whenever a class apply
_<<Contract>>_ stereotype, this means we are going to categorize it as smart contract for our targeted
blockchain platforms. Narratively, the purchase request developed and sent to the purchasing and
the financial departments. When the two departments approve the purchase request, they send to the
suppliers a request for quotations. The _Quotation_ adopts _<<Contract>>_ stereotype and therefore
quotation is now considered as a smart contracts. More over, the suppliers send their quotations to
the company, then the company evaluates the respondent suppliers and their product qualities.

![picture](Fig_appendix_1.png)
```
Fig Appendix.1 Class Diagram for Supply Chain sample
```
# B. Class Diagram Modelling

In Figure Appendix 1, we create class diagram by using Eclipse with our Blockchain Toolbox plugin
which we have created in this paper. We create 9 classes as depicted above (Figure Appendix 1) and
it is saved as SupplyChain_Demo_Sample.uml. We employ the << _Contract>>_ stereotype into 5
classes, (1) Quotation class modeled as part of the blockchain, the smart contract. (2) The Shipping
Method, (3) Contract classes are also deriving the BlockchainToolbox which came from the
datalogical interface. The other 2 classes are (4) _DirectShipping_ and (5) _FreightForwarder_ that
employ _<<Struct>>_ derived from _<<Contract>>_ stereotype.
After choosing one of the suppliers, the negotiation process started and once it is finalized, a smart
contract based _Contract_ with _<<Contract>>_ stereotype, will be issued and signed. The purchase
request will be transformed into the purchase order and the company chooses the _ShippingMethod_
for the purchased products by applying the <<Contract>> stereotype either by _DirectShipping_ or
_FreightForwarder_. Both these stereotypes applied _<<Struct>>_ stereotypes derived from
_<<Contract>>_ stereotype.

# C. Code Generation for Targeted Blockchain Platforms

First, we select our input by choosing the SupplyChain_Demo_Sample.uml. Secondly, we input
parameters for the targeted blockchain platform and run the generate button with steps :


- **Hyperledger Fabric** : We define the (1) base target directory: the place where we are going to place
    the generated Hyperledger Fabric source code. (2) The package prefix that organizes set of related
    classes and interfaces.
- **Ethereum** : Here we select the option to generate solidity language for the model we have created.
    (1) base target directory, (2) solidity version.

![Alt Text](/gifs/BlockchainToolbox-Run-Configurations.gif)

![picture](Fig_appendix_2.png)
```
Fig Appendix.2 BlockchainToolbox platform configuration
```

![picture](Fig_appendix_3.png)
```
Fig Appendix.3 Ethereum Solidity generated source code 
```

![picture](Fig_appendix_2.png)
```
Fig Appendix.4 Hyperledger Fabric generated source code
```

# D. Run Generated Code on Target Blockchain Platform

We generate the required files from the class diagram, the generated files and directories targeted
for both platforms based on the class diagram file, SupplyChain_Demo_Sample.uml. We publish the
source codes sample and the eclipse plugin site at: git@gitlab.com:Syahputra/SupplyChain-
Blockchain-Demo-Tilburg.git.

![Alt Text](/gifs/BlockchainToolbox-Run-Chaincode.gif)

For the ethereum platform, the generated files and directories is as illustrated in figure Appendix 3.
The main source code, SupplyChainDemo.sol is the outcome from this generation process.
Hyperledger Fabric, as it is using the Java API from Fabric Java SHIM make use of Java as targeted
language. In this Figure Appendix 4, we can se that the process generates base java code and the
gradle configuration files. 

![Alt Text](/gifs/BlockchainToolbox-Block-Transactions.gif)

We use this gradle configuration to run the smart contract in peer of
hyperledger blockchain. Beside peer code configuration file and logic, the client source code with
_REST-PROJECT-_ **_Class_Name_** - _SOAPUI-PROJECT_ .xml prefix template is also produced by our
generator.

# E. How to Create New Project
![Alt Text](/gifs/BlockchainToolbox-Create-New-Project.gif)


